import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import storage.DBRecordStorage;
import utils.JobInitializer;

import java.sql.ResultSet;
import java.sql.SQLException;


public class Application {
    public static void main(String[] args) throws SchedulerException, SQLException {
        if (args.length > 0 && args[0].equals("-p")) {
            ResultSet resultSet = DBRecordStorage.getInstance().getRecords();

            while (resultSet.next()) {
                System.out.println(resultSet.getString(1) + "\t\t|\t" + resultSet.getString(2));
            }

            System.exit(0);
        }
        else
        {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            JobInitializer.initializeBasic(scheduler);

            scheduler.start();
        }
    }
}