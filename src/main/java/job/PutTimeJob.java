package job;

import org.quartz.*;
import storage.DBRecordStorage;
import storage.LocalRecord;
import storage.LocalRecordStorage;

import java.util.List;

public class PutTimeJob implements Job {
    private static final LocalRecordStorage LOCAL_STORAGE = LocalRecordStorage.getInstance();
    private static DBRecordStorage DB_STORAGE;

    static {
        try {
            DB_STORAGE = DBRecordStorage.getInstance();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<LocalRecord> localRecords = LOCAL_STORAGE.getLocalRecords();

        localRecords.stream().filter(localRecord -> !localRecord.isSaved()).forEach(localRecord -> {
            if (DB_STORAGE.isAlive() && DB_STORAGE.save(localRecord)) {
                System.out.println(localRecord.getLocalDateTime());
            } else {
                try {
                    if (DB_STORAGE.getReconnectScheduler().getTriggerState(TriggerKey.triggerKey("reconnectTrigger")) == Trigger.TriggerState.PAUSED) {
                        DB_STORAGE.getReconnectScheduler().resumeJob(JobKey.jobKey("reconnect"));
                    }
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        });

        localRecords.removeIf(LocalRecord::isSaved);
    }
}
