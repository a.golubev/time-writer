package job;

import org.quartz.*;
import storage.DBRecordStorage;

public class ReconnectJob implements Job {
    private static DBRecordStorage DB_STORAGE;

    static {
        try {
            DB_STORAGE = DBRecordStorage.getInstance();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        if (!DB_STORAGE.checkConnection()) {
            DB_STORAGE.connect();
        } else {
            try {
                DB_STORAGE.getReconnectScheduler().pauseJob(JobKey.jobKey("reconnect"));
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
    }
}
