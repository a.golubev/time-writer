package job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import storage.LocalRecord;
import storage.LocalRecordStorage;

import java.time.LocalDateTime;


public class TraceTimeJob implements Job {
    private static final LocalRecordStorage LOCAL_STORAGE = LocalRecordStorage.getInstance();

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        LOCAL_STORAGE.getLocalRecords().add(new LocalRecord(LocalDateTime.now()));
    }
}
