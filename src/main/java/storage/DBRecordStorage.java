package storage;

import job.ReconnectJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import java.sql.*;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class DBRecordStorage {
    private static DBRecordStorage instance = null;

    private static final String DB_URL = "jdbc:mysql://localhost:3306/time_tracer?useSSL=false";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "Gffoe4!ssK";

    private Scheduler scheduler;
    private Connection connection;
    private boolean alive;

    private DBRecordStorage() throws SchedulerException {
        connect();
        initSheduller();
    }

    public static DBRecordStorage getInstance() throws SchedulerException {
        if (null == instance) {
            instance = new DBRecordStorage();
        }

        return instance;
    }

    public void connect() {
        try {
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            alive = true;
        } catch (SQLException e) {
            e.printStackTrace();
            alive = false;
        }
    }

    private void initSheduller() throws SchedulerException {
        scheduler = StdSchedulerFactory.getDefaultScheduler();

        JobDetail reconnect = newJob(ReconnectJob.class)
                .withIdentity("reconnect")
                .build();

        Trigger reconnectTrigger = newTrigger()
                .withIdentity("reconnectTrigger")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(5)
                        .repeatForever())
                .build();

        scheduler.scheduleJob(reconnect, reconnectTrigger);
    }

    public boolean isAlive() {
        return alive;
    }

    public Scheduler getReconnectScheduler() {
        return scheduler;
    }

    public boolean checkConnection() {
        try {
            Statement statement = connection.createStatement();
            statement.executeQuery("SELECT 1");
            alive = true;
        } catch (SQLException e) {
            alive = false;
        }

        return isAlive();
    }

    public boolean save(LocalRecord localRecord) {
        try {
            Statement statement = connection.createStatement();
            statement.execute("insert into date_time(date_time) values('" +
                    Timestamp.valueOf(localRecord.getLocalDateTime()) + "');");
            localRecord.setSaved(true);

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            alive = false;

            return false;
        }
    }

    public ResultSet getRecords() throws SQLException {
        Statement statement = connection.createStatement();

        return statement.executeQuery("select * from date_time");
    }
}
