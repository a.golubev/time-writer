package storage;

import java.time.LocalDateTime;

public class LocalRecord {
    private LocalDateTime localDateTime;
    private boolean saved;

    public LocalRecord(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
        saved = false;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }
}
