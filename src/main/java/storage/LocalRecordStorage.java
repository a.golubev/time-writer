package storage;

import java.util.ArrayList;
import java.util.List;

public class LocalRecordStorage {
    private static LocalRecordStorage instance = null;

    private List<LocalRecord> localRecords;

    private LocalRecordStorage() {
        localRecords = new ArrayList<>();
    }

    public static LocalRecordStorage getInstance() {
        if (null == instance) {
            instance = new LocalRecordStorage();
        }

        return instance;
    }

    public List<LocalRecord> getLocalRecords() {
        return localRecords;
    }
}
