package utils;

import job.PutTimeJob;
import job.TraceTimeJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class JobInitializer {
    private JobInitializer() {}

    public static void initializeBasic(Scheduler scheduler) throws SchedulerException {
        initTraceTimeJob(scheduler);
        initPutTimeJob(scheduler);
    }

    private static void initTraceTimeJob(Scheduler scheduler) throws SchedulerException {
        JobDetail traceTime = newJob(TraceTimeJob.class)
                .withIdentity("traceTime")
                .build();

        Trigger traceTimeTrigger = newTrigger()
                .withIdentity("traceTimeTrigger")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(1)
                        .repeatForever())
                .build();

        scheduler.scheduleJob(traceTime, traceTimeTrigger);
    }

    private static void initPutTimeJob(Scheduler scheduler) throws SchedulerException {
        JobDetail putTime = newJob(PutTimeJob.class)
                .withIdentity("putTime")
                .build();

        Trigger putTimeTrigger = newTrigger()
                .withIdentity("putTimeTrigger")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(1)
                        .repeatForever())
                .build();

        scheduler.scheduleJob(putTime, putTimeTrigger);
    }
}
