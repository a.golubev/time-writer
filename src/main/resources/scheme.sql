-- CREATE DATABASE time_tracer;
-- USE time_tracer;

CREATE TABLE date_time (
    id bigint unsigned not null auto_increment,
    date_time TIMESTAMP not null,
    constraint pk_date_time primary key (id)
);